#STDIO over TCP (WinSock2)#

*Windows version.*

*Copyright (c) 2016 Ivane Gegia http://ivane.info*

**Programming language:** C

**Library prefix:** SOT

**License:** MIT (see [LICENSE.md](LICENSE.md))

**Version:** 1.0 beta

#Usage:#

To run client:
```
STDIOTCP.exe --remote 127.0.0.1 8090
```

Run server:
```
STDIOTCP.exe --port 8090
```

#Client/Server example:#

This way client.exe and server.exe can communicate via STDIO over network(TCP).

```
server.exe | STDIOTCP.exe --port 8090
```

```
client.exe | STDIOTCP.exe --remote 127.0.0.1 8090
```

![Doc.PNG](Doc.PNG)

#[Download v1.0 beta](https://bitbucket.org/ivaneg/stdiotcp/downloads)#