#include "stdiotcp.h"
#include <string.h>
#include <stdlib.h>

int __cdecl main(int argc, char **argv)
{
#define SOT_COMMAND 1

    const char cmd_port[] = "--port";
    const char cmd_remoteIP[] = "--remote";


    if (argc < 3)
    {
        return 0;
    }

    if (strcmp(argv[SOT_COMMAND], cmd_port) == 0)
    {
        SOTSTATUS sotStatus = SOTCreateServer(argv[2]);
    }
    else if (strcmp(argv[SOT_COMMAND], cmd_remoteIP) == 0)
    {
        SOTSTATUS sotStatus = SOTCreateClient(argv[2], argv[3]);
    }


    return 0;
}