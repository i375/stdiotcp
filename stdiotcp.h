/*
Copyright(c) 2016 Ivane Gegia http ://ivane.info  

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files(the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions :

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#pragma once

#undef UNICODE

#define WIN32_LEAN_AND_MEAN

#include <Windows.h>
#include <WinSock2.h>
#include <WS2tcpip.h>
#include <stdlib.h>
#include <stdio.h>

#pragma comment (lib, "Ws2_32.lib")

// სოკეტით, ბაიტების მიმღებული ბუფერის ზომა
#define SOT_SEND_RECV_BUFFER_LEN 512

typedef enum 
{
    // პრობლემები არ დაფიქსირებულა
    SOTSTATUS_OK,
    // დაფიქსირედა რაიმე პრობლემა
    SOTSTATUS_ERROR
}SOTSTATUS;

// STDIO over TCP-ს აღმწერი სტრუქტურა
typedef struct SOT
{
    // WinSock-ის აღმწერი სტრუქტურა
    WSADATA wsaData; 
    // სხვადასხვა სოკეტ ფუნქციონალის შედეგის კოდების შემნახველი ველი
    int iResult;
    // სერვერული სოკეტი
    SOCKET serverSocket;
    // კლიენტ სუკეტი
    SOCKET clientSocket;
    // "ip":"port" ტექსტი თარგმნილი მისამართის სტრუქტურაში
    struct addrinfo *addrResults;
    // სტრუტქრუა რომელიც გამოიყენება addrResults-ებში იტერაციისთვის
    struct addrinfo *addrResultsIterator;
    // მისამართის ტიპის კარნახები getaddrinfo-სთვის
    struct addrinfo addrHints;
    // ბაიტების მიმღები ბუფერი
    // +1 NULL ტერმინატორისთვის
    char recvBuffer[SOT_SEND_RECV_BUFFER_LEN + 1];

    // გაგზავნის ბუფერი
    // +1 NULL ტერმინატორისთვის
    char sendBuffer[SOT_SEND_RECV_BUFFER_LEN + 1];
    // recv-ის მიერ მიღებული ბაიტების ოდენბა ან სტატუს კოდები
    int bytesReceivedOrError;
}SOT;

// ფუნქციების პროტოტიპები
static void SOTZeroSendRecvBuffers(_Inout_ SOT* sot);


// აინიციალიზირებს SOT სტრუქტურას საწყისი მონაცემებით
static void SOTInit(_Inout_ SOT* sot)
{
    // STDIO-ს ბუფერიზაციის გაუქმება
    setvbuf(stdin, NULL, _IONBF, 0);
    setvbuf(stdout, NULL, _IONBF, 0);

    sot->serverSocket = INVALID_SOCKET;
    sot->clientSocket = INVALID_SOCKET;
    sot->addrResults = NULL;
    SOTZeroSendRecvBuffers(sot);
}

static void SOTZeroSendRecvBuffers(_Inout_ SOT* sot)
{
    ZeroMemory(sot->sendBuffer, SOT_SEND_RECV_BUFFER_LEN + 1);
    ZeroMemory(sot->recvBuffer, SOT_SEND_RECV_BUFFER_LEN + 1);
}

// ნაკადი რომელიც უსმენს stdin-ს და მიღებულ ტექსტს აგზავნის socket send-ით
static DWORD WINAPI SOTServerSendThread(_In_ SOT* sot)
{

    while (TRUE)
    {
        SOTZeroSendRecvBuffers(sot);
        fgets(sot->sendBuffer, SOT_SEND_RECV_BUFFER_LEN, stdin);

        sot->iResult = send(sot->clientSocket, sot->sendBuffer, (int)strlen(sot->sendBuffer), 0);

        if (sot->iResult == SOCKET_ERROR)
        {
            printf("socket send failed with error: %d\n", WSAGetLastError());
            closesocket(sot->clientSocket);
            WSACleanup();
            return 1;
        }
    }

    return 0;
}

SOTSTATUS SOTCreateServer(_In_ char* portNumber)
{
    SOT *sot = (SOT*)malloc(sizeof(SOT));

    if (sot == NULL)
    {
        return SOTSTATUS_ERROR;
    }

    SOTInit(sot);

    // ვსტარტავ WinSock2-ს
    sot->iResult = WSAStartup(MAKEWORD(2, 2), &sot->wsaData);
    
    if (sot->iResult != 0)
    {
        printf("Couldn't initialize WinSock2\n");
        return SOTSTATUS_ERROR;
    }

    // getaddrinf-სთვის ვამზადებ სამხმარე ინფორმაციას/პარამეტრებს
    ZeroMemory(&sot->addrHints, sizeof(sot->addrHints));
    sot->addrHints.ai_family = AF_INET;
    sot->addrHints.ai_socktype = SOCK_STREAM;
    sot->addrHints.ai_protocol = IPPROTO_TCP;
    sot->addrHints.ai_flags = AI_PASSIVE;

    // მოვიპოვებ მისამართის სტრქუტურას რომელზეც მივაბამ სერვერ სოკეტს
    sot->iResult = getaddrinfo(NULL, portNumber, &sot->addrHints, &sot->addrResults);
    
    if (sot->iResult != 0)
    {
        printf("Couldn't resovle address information\n");
        WSACleanup();
        return SOTSTATUS_ERROR;
    }

    // ვქმნი სოკეტს getaddrinfo-ს მიერ მოწოდებული მონაცემებით
    sot->serverSocket = socket(sot->addrResults[0].ai_family, sot->addrResults[0].ai_socktype, sot->addrResults[0].ai_protocol);

    if (sot->serverSocket == INVALID_SOCKET)
    {
        printf("Failed creating serve socket with error: %ld\n", WSAGetLastError());
        freeaddrinfo(sot->addrResults);
        closesocket(sot->serverSocket);
        WSACleanup();

        return SOTSTATUS_ERROR;
    }

    // ვაბამ სერვერულ სოკეტს, მისამართის მონაცემებს
    sot->iResult = bind(sot->serverSocket, sot->addrResults[0].ai_addr, (int)sot->addrResults[0].ai_addrlen);

    if (sot->iResult == SOCKET_ERROR)
    {
        printf("Binding socket failed with errro: %d", WSAGetLastError());
        freeaddrinfo(sot->addrResults);
        closesocket(sot->serverSocket);
        WSACleanup();
        return SOTSTATUS_ERROR;
    }

    // ვათავისუფლებ sot->addrResults-ის მიერ დაკავებულ მეხსიერებას, რახან აღარ მჭირდება
    freeaddrinfo(sot->addrResults);

    // ვიწყებ სერვერულ სოკეტზე შეერთებების სმენას
    sot->iResult = listen(sot->serverSocket, SOMAXCONN);

    if (sot->iResult == SOCKET_ERROR)
    {
        printf("Failed to listen on server socket with error: %d", WSAGetLastError());
        closesocket(sot->serverSocket);
        WSACleanup();
        return SOTSTATUS_ERROR;
    }
    
    // ვიწკებ კლიენტ შეერთებების მიღებას, სერვერულ სოკეტზე
    sot->clientSocket = accept(sot->serverSocket, NULL, NULL);

    if (sot->clientSocket == INVALID_SOCKET)
    {
        printf("Failed accpeting client on socket with error: %ld", WSAGetLastError());
        closesocket(sot->serverSocket);
        WSACleanup();
        return SOTSTATUS_ERROR;
    }

    // ვხურავ სერვერულ სოკეტს, რაღან მეტი კლიენტ შეერთებების მიღება არ მინდა
    closesocket(sot->serverSocket);

    HANDLE sendThread = CreateThread(NULL, 0, SOTServerSendThread, sot, 0, NULL);

    do
    {
        // ველოდები ბაინტებს შემოსულს კლიენტ სოკეტზე
        SOTZeroSendRecvBuffers(sot);
        sot->bytesReceivedOrError = recv(sot->clientSocket, sot->recvBuffer, SOT_SEND_RECV_BUFFER_LEN, 0);

        if (sot->bytesReceivedOrError > 0)
        {
            //printf("Bytes received: %d\n", sot->bytesReceivedOrError);
            
            fputs(sot->recvBuffer, stdout);
            SOTZeroSendRecvBuffers(sot);
            
        }
        // როცა მიღებული ბაიტების ოდენობა 0-ის ტოლია, ნიშნავს რომ კლიენტ სოკეტზე შეერთება დაიხრურა
        else if (sot->bytesReceivedOrError == 0)
        {
            printf("Client connection closing\n");
        }
        // შეცდომების დამუშავება
        else
        {
            printf("recv failure with error: %ld\n", WSAGetLastError());
            closesocket(sot->clientSocket);
            WSACleanup();
            return SOTSTATUS_ERROR;
        }

    } while (sot->bytesReceivedOrError > 0);

    // ვაუქმებ send ოპერაციების მიეღებას კლიენტ სოკეტზე
    sot->iResult = shutdown(sot->clientSocket, SD_SEND);
    if (sot->iResult == SOCKET_ERROR)
    {
        printf("Shutdown on client socket failed with error: %ld\n", WSAGetLastError());
        closesocket(sot->clientSocket);
        WSACleanup();
        return SOTSTATUS_ERROR;
    }

    closesocket(sot->clientSocket);
    WSACleanup();

    free(sot);

    return SOTSTATUS_OK;
}

// პარარელური ნაკადი რომელიც "უსმენს" სერვერიდან შემოსულ ინფორმაციას
static DWORD WINAPI SOTClientRecvThread(_In_ SOT* sot)
{

    // ველოდები სერვერიდან შემოსულ ბაიტებს
    // ან სერვეის მიერ შეერთების დახურვას
    // ან შეცდომას
    do
    {
        SOTZeroSendRecvBuffers(sot);
        sot->iResult = recv(sot->clientSocket, sot->recvBuffer, SOT_SEND_RECV_BUFFER_LEN, 0);

        if (sot->iResult > 0)
        {
            fputs(sot->recvBuffer, stdout);
            //printf("bytes received on client socket: %d\n", sot->iResult);
        }
        else if (sot->iResult == 0)
        {
            printf("Server closed connection.\n");
        }
        else
        {
            printf("Socket recv failed with error: %d\n", WSAGetLastError());
        }

    } while (sot->iResult > 0);

    return 0;
}

// SOT კლიენტის შემქნა
SOTSTATUS SOTCreateClient(_In_ char* remoteIPAddress, _In_ char* remotePortNumber)
{
    SOT *sot = (SOT*)malloc(sizeof(SOT));

    if (sot == NULL)
    {
        return SOTSTATUS_ERROR;
    }

    SOTInit(sot);

    // ვსტარტავ WinSock-ს
    sot->iResult = WSAStartup(MAKEWORD(2, 2), &sot->wsaData);
    
    if (sot->iResult != 0)
    {
        printf("WSAStartup failed with result %d\n", sot->iResult);
        return SOTSTATUS_ERROR;
    }

    ZeroMemory(&sot->addrHints, sizeof(sot->addrHints));
    sot->addrHints.ai_family = AF_UNSPEC;
    sot->addrHints.ai_socktype = SOCK_STREAM;
    sot->addrHints.ai_protocol = IPPROTO_TCP;

    // ვარკვევ სერვერის მისამართს
    // ამ შემთხვევაში შეიძლება sot->addrResults იყოს 0-ზე მეტი ოდენობის, ანუ : sot->addrResults[0], sot->addrResults[1], sot->addrResults[...]
    sot->iResult = getaddrinfo(remoteIPAddress, remotePortNumber, &sot->addrHints, &sot->addrResults);

    if (sot->iResult != 0)
    {
        printf("getaddrinfo failed to resolve server address, with error: %d", sot->iResult);
        WSACleanup();
        return SOTSTATUS_ERROR;
    }

    // getaddrinfo-ს მიერ დაბრუნებული შესაძლო სერვერის მისამართებზე
    // ვცდილობ შეერთებას და პირველი წარმატუებული შეერთებისას ვწყვეტ მცდელობებს
    for (
        sot->addrResultsIterator = sot->addrResults;
        sot->addrResultsIterator != NULL;
        sot->addrResultsIterator = sot->addrResults->ai_next)
    {
        sot->clientSocket = socket(
            sot->addrResultsIterator->ai_family,
            sot->addrResultsIterator->ai_socktype,
            sot->addrResultsIterator->ai_protocol);

        if (sot->clientSocket == INVALID_SOCKET)
        {
            printf("socket creation failed with error: %ld\n", WSAGetLastError());
            WSACleanup();
            return SOTSTATUS_ERROR;
        }

        // ვცდი დავამყარო სოკეტ შეერთება
        sot->iResult = connect(sot->clientSocket, sot->addrResultsIterator->ai_addr, (int)sot->addrResultsIterator->ai_addrlen);

        // თუ კავშირი ვერ დამყარდა მაშინ ვახტები ციკლს და ვცდილობ შემდეგ მისამართზე შეერთებას
        if (sot->iResult == SOCKET_ERROR)
        {
            closesocket(sot->clientSocket);
            sot->clientSocket = INVALID_SOCKET;
            continue;
        }

        break;
    }

    freeaddrinfo(sot->addrResults);

    // შეერთების მცდელობების ციკლის დასრულებისას შეიძება სოკეტ შეერთება დამყარდეს
    // ან არ იყოს დამყარებული, აქ ვამოწმებ შეერთების ვერ დამყარების შემთხვევას
    if (sot->clientSocket == INVALID_SOCKET)
    {
        printf("Unable to connect to server! \n");
        WSACleanup();
        return SOTSTATUS_ERROR;
    }

    // სერვერიდან მოსული ინფორმაციის დამმუშავებელი პარარელური ნაკადი
    HANDLE recvThread = CreateThread(NULL, 0, SOTClientRecvThread, sot, 0, NULL);

    // შეერთების წარმატებით დამყარების შემთხვევაში, ვაგზავნი ინფორმაციას
    // TODO: მომავალში აქ ჩაჯდება STDIO over TCP ციკლი
    while (TRUE)
    {
        fgets(sot->sendBuffer, SOT_SEND_RECV_BUFFER_LEN, stdin);

        sot->iResult = send(sot->clientSocket, sot->sendBuffer, (int)strlen(sot->sendBuffer), 0);

        if (sot->iResult == SOCKET_ERROR)
        {
            printf("socket send failed with error: %d\n", WSAGetLastError());
            closesocket(sot->clientSocket);
            WSACleanup();
            return SOTSTATUS_ERROR;
        }

        //printf("socket send bytes send: %ld\n", sot->iResult);
    }

    sot->iResult = shutdown(sot->clientSocket, SD_SEND);

    if (sot->iResult == SOCKET_ERROR)
    {
        printf("Shutting SEND on client socket failed with error: %ld\n", WSAGetLastError());
        closesocket(sot->clientSocket);
        WSACleanup();
        return SOTSTATUS_ERROR;
    }
    
    closesocket(sot->clientSocket);
    WSACleanup();

    free(sot);

    return SOTSTATUS_OK;
}
